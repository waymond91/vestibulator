# Vestibulator
This is source code for a sensory substitution device I designed for my mother who suffers from vestibular neuronitis.
The disorder has prevented her from participating in a lot of
The concept is to simply to attach a small IMU to the back small patch of skin behind the ear and to use this to create a new pathway for her heads motion data to enter the brain. This device then captures motion data from her head and maps to set of daisy-chained vibration motors on the small of her back.
This work is inspired by David Eagleman's work with deaf people.

## David Eagleman - Sensory Substitution
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/4c1lqFXHvqI/0.jpg)](https://www.youtube.com/watch?v=4c1lqFXHvqI)

## Vibration Motor PCBs
![Oshpark Gerbers](https://i.ibb.co/17wz8vj/Screenshot-from-2020-10-03-16-46-36.png)  
![Daisy Chained Motors + Sensor](https://i.ibb.co/9cNmmrR/Whats-App-Image-2020-10-03-at-4-51-19-PM.jpg)
## Challenges:
The main challenge with a project like this beyond funding, construction, and testing, is finding the best way to map the IMU data dimensions to the vibration motors. What is the minunum number of possible vibration motors? Should the data be interpreted into a AHRS, or left raw? How to best represent positive and negative values? 

The inner ear actually perceives motion very similar to a MEMs gyroscope and accelerometer. The mechanics are almost identical. So far I've been developing the solution with 12 degrees of freedom. 6 for the 3 axis of accelerometer data (+/-) and another 6 for the 3 axis gyroscope data.

These are scaled logarithmically, as most of the human senses are perceived on a log scale.
