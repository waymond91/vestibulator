#include <mbed.h>
#include <math.h>
Serial pc(USBTX, USBRX);

class ShiftPWM
    {
    private:
		Ticker ticker;
		Timer timer;
		
		volatile int num_chans;
		volatile int pwm_period;
		volatile bool is_start = true;
		volatile int next;
	
        DigitalOut* dout;
        DigitalOut* clock;
        DigitalOut*	clear;	
        DigitalOut* set;
        
 
    public:
        ShiftPWM(PinName, PinName, PinName, PinName, int, int);
        ~ShiftPWM();
        void init();
        void shift_out();
        void isr();
        int find_next(float);
        
        volatile bool flag = false;
        volatile int* register_states = NULL;
        volatile int* up_times = NULL;
        volatile float* duty_cycles = NULL;

    
 }; 
 
 ShiftPWM::~ShiftPWM(){
	delete dout;
	delete clock;
	delete clear;
	delete set;
	delete register_states;
	delete up_times;
	delete duty_cycles;
 }
 
ShiftPWM::ShiftPWM(PinName _dout, PinName _clock, PinName _clear, PinName _set, int _num_chans, int period){
	dout = new DigitalOut(_dout);
	clock = new DigitalOut(_clock);
	clear = new DigitalOut(_clear);
	set = new DigitalOut(_set);
	
	pwm_period = period;
	num_chans = _num_chans;	
	register_states = new int[num_chans];
	up_times = new int[num_chans];
	duty_cycles = new float[num_chans];
}

void ShiftPWM::shift_out(){
	clear -> write(0);
	clear -> write(1);
	
	for (int j = 0; j < num_chans; j++){
		dout -> write(register_states[j]);
		clock -> write(0);
		clock -> write(1);
	}
	
	set -> write(1);
	set -> write(0);
}

void ShiftPWM::init(){
	flag = true;
	timer.start();
	ticker.attach_us(this, &ShiftPWM::isr, 1.0);
}

void ShiftPWM::isr(){
	float time;
    
    ticker.detach();
    
    if (is_start == true){
        if (flag == true){
            for(int j =0; j< num_chans; j++){
                up_times[j] = duty_cycles[j]*pwm_period;
            }
        flag = false;
        }
        
        timer.reset();
        is_start = false;
        
        for(int j = 0; j<num_chans; j++){
            if(up_times[j] > 0.0){
                register_states[j] = 1;
            }else{
                register_states[j] = 0;
            }
        }
        
        shift_out();
        time = timer.read_us();
        next = find_next(time);
        
        if (next != -1){
            ticker.attach_us(this, &ShiftPWM::isr, up_times[next] - time);
            return;
        }else{
            is_start = true;
            ticker.attach_us(this, &ShiftPWM::isr, pwm_period - time);
            return;
        }    
        
                
    }else{
        register_states[next] = 0;
        shift_out();
        time = timer.read_us();
        next = find_next(time);
        if (next != -1){
            ticker.attach_us(this, &ShiftPWM::isr, up_times[next] - time);
            return;
        }else{
            is_start = true;
            ticker.attach_us(this, &ShiftPWM::isr, pwm_period - time);
            return;
        }      
    } 
}

int ShiftPWM::find_next(float time){
	float min_val = pwm_period;
    int next_index = -1;
    for (int j = 0; j< num_chans; j++){
        if(up_times[j] < min_val && up_times[j] > time){
            min_val = up_times[j];
            next_index = j;
        }
    }
    return next_index;
}

int main() {
	
	ShiftPWM motors(A2, A1, A3, A0, 2, 35000);
	motors.duty_cycles[0] = 0.0;
	motors.duty_cycles[1] = 0.5;
	motors.init();
	while(1){
		for(float j = 0.25; j<0.5; j+= 0.0001){
            motors.duty_cycles[1] = 0;
            motors.duty_cycles[0] = abs(1-(exp(j))); 
            motors.flag = true;
            wait_ms(10);
        }
	}
}

